#!/usr/bin/python3


from xml.dom.minidom import parse
import sys
import urllib.request

if len(sys.argv) < 2 or len(sys.argv) > 2:
    print("Usage: python3 ytparser.py <id del canal>")
    print()
    print(" <id del canal>: identificador del canal de YouTube a parsear")
    sys.exit(1)


PAGE1 = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Channel contents:</h1>
    <ul>"""

PAGE2 = """
    </ul>
  </body>
</html>
"""

url_yt = 'https://www.youtube.com/feeds/videos.xml?channel_id=' + sys.argv[1]

ytXmlFile = urllib.request.urlopen(url_yt)  # abre el doc xml de youtube

print(PAGE1)

with ytXmlFile as file:
    document = parse(ytXmlFile)  # Parsea el doc xml con parser dom (lo hace solo, para dom siempre es igual)
    videos = document.getElementsByTagName('entry')  # devuelve una lista de objetos con elemento entry
    for video in videos:
        titles = video.getElementsByTagName('title')  # elementos con eiqueta title
        links = video.getElementsByTagName('link')  # elementos con etiqueta link
        for link in links:
            enlace = link.getAttribute('href')  # para cada link, obtiene su atributo href
        print(f"        <li><a href='{enlace}'>{titles[0].firstChild.nodeValue}.</a></li>")

print(PAGE2)
